#include <iostream>
#include <ctime>
#include <windows.h>
using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
		const int SIZE = 3;
		int	arr[SIZE][SIZE];
		int	arr1[SIZE][SIZE];

		cout << "Первий масив" << endl;
		for (int i = 0; i < SIZE; i++)
		{
			for (int j = 0; j < SIZE; j++)
			{
				arr[i][j] = rand() % 10;
				cout << arr[i][j] << "\t";
			}
			cout << endl;
		}
		cout << "Другий масив" << endl;
		for (int i = 0; i < SIZE; i++)
		{
			for (int j = 0; j < SIZE; j++)
			{
				arr1[i][j] = rand() % 10;
				cout << arr1[i][j] << "\t";
			}
			cout << endl;
		}
		cout << "Сума масивів" << endl;
		cout << arr[0][0] + arr1[0][0] << "\t" << arr[0][1] + arr1[0][1] << "\t" << arr[0][2] + arr1[0][2] << endl;
		cout << arr[1][0] + arr1[1][0] << "\t" << arr[1][1] + arr1[1][1] << "\t" << arr[1][2] + arr1[1][2] << endl;
		cout << arr[2][0] + arr1[2][0] << "\t" << arr[2][1] + arr1[2][1] << "\t" << arr[2][2] + arr1[2][2] << endl;

		system("pause");
