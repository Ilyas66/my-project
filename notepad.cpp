#include <iostream>
#include <windows.h>
#include <algorithm>

using namespace std;
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	string reverse_str = "Строка";
	reverse(reverse_str.begin(), reverse_str.end());
	// first та end використовується для перевернення файла. Тобто, завдяки ним і відбувається процес перевертання строки.
	cout << reverse_str << endl;

	system("pause");
	return 0;
}
